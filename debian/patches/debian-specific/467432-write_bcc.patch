From: Antonio Radici <antonio@debian.org>
Date: Thu, 27 Feb 2014 16:22:51 +0100
Subject: 467432-write_bcc
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

The purpose of this patch is to alter the behavior of the write_bcc option
because exim4, the default SMTP in Debian, does not strip the Bcc headers; by
default write_bcc is set so this could cause a privacy leak.

The behavior that this patch introduces is: never write the Bcc header when the
message is sent, otherwise, if the message is sent to Fcc, then this optin
will be evaluated and the Bcc header will be written based on that.

Background discussion on this is in the following bugs:
http://bugs.debian.org/304718
http://bugs.debian.org/467432

This patch is a slightly modified version of the patch provided by
Stefan Völkel <stefan@bc-bd.org> in the second bug.
---
 headers.c |  2 +-
 init.h    | 11 +++--------
 main.c    |  2 +-
 protos.h  |  2 +-
 send.c    |  4 ++--
 sendlib.c |  6 +++---
 6 files changed, 11 insertions(+), 16 deletions(-)

--- a/init.h
+++ b/init.h
@@ -349,9 +349,6 @@
   ** .pp
   ** When this variable is \fIset\fP, mutt will include Delivered-To headers when
   ** bouncing messages.  Postfix users may wish to \fIunset\fP this variable.
-  ** .pp
-  ** \fBNote:\fP On Debian systems, this option is unset by default in
-  ** /etc/Muttrc.
   */
   { "braille_friendly", DT_BOOL, R_NONE, OPTBRAILLEFRIENDLY, 0 },
   /*
@@ -785,7 +782,7 @@
   ** .pp
   ** This variable specifies which editor is used by mutt.
   ** It defaults to the value of the \fC$$$VISUAL\fP, or \fC$$$EDITOR\fP, environment
-  ** variable, or to the string ``/usr/bin/editor'' if neither of those are set.
+  ** variable, or to the string ``vi'' if neither of those are set.
   ** .pp
   ** The \fC$$editor\fP string may contain a \fI%s\fP escape, which will be replaced by the name
   ** of the file to be edited.  If the \fI%s\fP escape does not appear in \fC$$editor\fP, a
@@ -1200,9 +1197,6 @@
   ** which case a detected one is not used.
   ** .pp
   ** Also see $$use_domain and $$hidden_host.
-  ** .pp
-  ** \fBNote:\fP On Debian systems, the default for this variable is obtained
-  ** from /etc/mailname when Mutt starts.
   */
 #if defined(HAVE_LIBIDN) || defined(HAVE_LIBIDN2)
   { "idn_decode",	DT_BOOL, R_MENU, OPTIDNDECODE, 1},
@@ -1820,9 +1814,6 @@
   ** system.  It is used with various sets of parameters to gather the
   ** list of known remailers, and to finally send a message through the
   ** mixmaster chain.
-  ** .pp
-  ** \fBNote:\fP On Debian systems, this option is set by default to
-  ** ``mixmaster-filter'' in /etc/Muttrc.
   */
 #endif
   { "move",		DT_QUAD, R_NONE, OPT_MOVE, MUTT_NO },
@@ -3340,10 +3331,6 @@
   ** This is a format string, see the $$smime_decrypt_command command for
   ** possible \fCprintf(3)\fP-like sequences.
   ** (S/MIME only)
-  ** .pp
-  ** \fBNote:\fP On Debian systems, this defaults to the first existing file in
-  ** the following list: ~/.smime/ca-certificates.crt ~/.smime/ca-bundle.crt
-  ** /etc/ssl/certs/ca-certificates.crt.
   */
 #ifdef USE_SMTP
 # ifdef USE_SASL
@@ -3503,9 +3490,6 @@
   ** .ts
   ** set ssl_ca_certificates_file=/etc/ssl/certs/ca-certificates.crt
   ** .te
-  ** .pp
-  ** \fBNote:\fP On Debian systems, this option is set by default to
-  ** ``threads'' in /etc/Muttrc.
   */
 #endif /* USE_SSL_GNUTLS */
   { "ssl_client_cert", DT_PATH, R_NONE, UL &SslClientCert, 0 },
@@ -4018,9 +4002,6 @@
   ** is set to deliver directly via SMTP (see $$smtp_url), this
   ** option does nothing: mutt will never write out the ``Bcc:'' header
   ** in this case.
-  ** .pp
-  ** \fBNote:\fP On Debian systems, exim4 and postfix strip BCC headers by
-  ** default. The above warning applies to exim3 users, see /etc/Muttrc.
   */
   { "write_inc",	DT_NUM,	 R_NONE, UL &WriteInc, 10 },
   /*
--- a/protos.h
+++ b/protos.h
@@ -381,7 +381,7 @@
 int mutt_write_mime_body (BODY *, FILE *);
 int mutt_write_mime_header (BODY *, FILE *);
 int mutt_write_one_header (FILE *fp, const char *tag, const char *value, const char *pfx, int wraplen, int flags);
-int mutt_write_rfc822_header (FILE *, ENVELOPE *, BODY *, int, int);
+int mutt_write_rfc822_header (FILE *, ENVELOPE *, BODY *, int, int, int);
 void mutt_write_references (LIST *, FILE *, int);
 int mutt_yesorno (const char *, int);
 void mutt_set_header_color(CONTEXT *, HEADER *);
--- a/sendlib.c
+++ b/sendlib.c
@@ -1969,7 +1969,7 @@
 
 
 int mutt_write_rfc822_header (FILE *fp, ENVELOPE *env, BODY *attach,
-			      int mode, int privacy)
+			      int mode, int privacy, int should_write_bcc)
 {
   char buffer[LONG_STRING];
   char *p, *q;
@@ -2012,7 +2012,7 @@
   else if (mode > 0)
     fputs ("Cc: \n", fp);
 
-  if (env->bcc)
+  if (env->bcc && should_write_bcc)
   {
     if(mode != 0 || option(OPTWRITEBCC))
     {
@@ -2804,7 +2804,7 @@
   /* post == 1 => postpone message. Set mode = -1 in mutt_write_rfc822_header()
    * post == 0 => Normal mode. Set mode = 0 in mutt_write_rfc822_header()
    * */
-  mutt_write_rfc822_header (msg->fp, hdr->env, hdr->content, post ? -post : 0, 0);
+  mutt_write_rfc822_header(msg->fp, hdr->env, hdr->content, post ? -post : 0, 0, 1);
 
   /* (postponment) if this was a reply of some sort, <msgid> contians the
    * Message-ID: of message replied to.  Save it using a special X-Mutt-
--- a/headers.c
+++ b/headers.c
@@ -53,7 +53,7 @@
   }
   
   mutt_env_to_local (msg->env);
-  mutt_write_rfc822_header (ofp, msg->env, NULL, 1, 0);
+  mutt_write_rfc822_header(ofp, msg->env, NULL, 1, 0, 1);
   fputc ('\n', ofp);	/* tie off the header. */
 
   /* now copy the body of the message. */
--- a/main.c
+++ b/main.c
@@ -1184,7 +1184,7 @@
           mutt_env_to_intl (msg->env, NULL, NULL);
         }
 
-        mutt_write_rfc822_header (fout, msg->env, msg->content, -1, 0);
+        mutt_write_rfc822_header(fout, msg->env, msg->content, -1, 0, 1);
         if (option (OPTRESUMEEDITEDDRAFTFILES))
           fprintf (fout, "X-Mutt-Resume-Draft: 1\n");
         fputc ('\n', fout);
--- a/send.c
+++ b/send.c
@@ -994,10 +994,10 @@
     unset_option (OPTWRITEBCC);
 #endif
 #ifdef MIXMASTER
-  mutt_write_rfc822_header (tempfp, msg->env, msg->content, 0, msg->chain ? 1 : 0);
+  mutt_write_rfc822_header(tempfp, msg->env, msg->content, 0, msg->chain ? 1 : 0, 0);
 #endif
 #ifndef MIXMASTER
-  mutt_write_rfc822_header (tempfp, msg->env, msg->content, 0, 0);
+  mutt_write_rfc822_header(tempfp, msg->env, msg->content, 0, 0, 0);
 #endif
 #ifdef USE_SMTP
   if (old_write_bcc)
